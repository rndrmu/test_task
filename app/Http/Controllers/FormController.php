<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FilledForm;
use App\Models\FilledForm as ModelsFilledForm;

class FormController extends Controller
{
    public function submit(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'street' => 'required',
            'postal_code' => 'required',
            'city' => 'required',
            'message' => 'required',
        ]);

        $form = new ModelsFilledForm();
        $form->fill($validatedData);
        $form->save();



        return response()->json([
            'message' => 'Contact saved successfully',
            'data' => $form,
        ], 201);
    }

    public function get_all()
    {
        $forms = ModelsFilledForm::all();
        return response()->json([
            'message' => 'All contacts',
            'data' => $forms,
        ], 200);
    }

    protected $resourceAbilityMap = [
        'store' => 'create',
    ];

}

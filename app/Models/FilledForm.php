<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilledForm extends Model
{

    protected $fillable = [
        'name',
        'email',
        'street',
        'postal_code',
        'city',
        'message',
    ];
}

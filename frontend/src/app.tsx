import { useState } from 'preact/hooks'
//@ts-ignore
import { useRouter, Route, Link } from "wouter-preact";
import localforage from 'localforage';
import './app.css'
import Home from './components/Home';
import About from './components/List';
import Login from './components/Login';

export function App() {
  const [loggedIn, setLoggedIn] = useState(false);

    localforage.getItem('authToken').then((value) => {
        if (value) {
            setLoggedIn(true);
        }
    }).catch((err) => {
        console.log(err);
    });

  return (
    <>
        <nav>
            <Link href="/">Home</Link>
            {
                loggedIn ? <Link href="/all">View all collected data</Link> : ''
            }
            <Link href="/login">Login</Link>
        </nav>

      <Route path="/"> <Home /> </Route>
      <Route path="/all"> <About /> </Route>
      <Route path="/login"> <Login /> </Route>
    </>
  )
}





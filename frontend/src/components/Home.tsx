import { useState } from "preact/hooks";
//@ts-ignore
import { useRouter } from "wouter-preact";

function Home() {
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        street: '',
        zip: '',
        city: '',
        message: '',
      });

      const router = useRouter();

      const handleSubmit = async (e: any) => {
        e.preventDefault();
        await fetch('/submit', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(formData),
        });
        router.push('/success');
      };

      const handleChange = (e: any) => {
        setFormData({
          ...formData,
          [e.target.name]: e.target.value,
        });
      };
    return (
        <>
        <form onSubmit={handleSubmit}>
        <div class="form">

          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            name="name"
            required
            onChange={handleChange}
            value={formData.name}
          />
          <label htmlFor="email">E-Mail:</label>
          <input
            type="email"
            id="email"
            name="email"
            required
            onChange={handleChange}
            value={formData.email}
          />
          <label htmlFor="street">Street:</label>
          <input
            type="text"
            id="street"
            name="street"
            required
            onChange={handleChange}
            value={formData.street}
          />
          <label htmlFor="zip">ZIP:</label>
          <input
            type="text"
            id="zip"
            name="zip"
            required
            onChange={handleChange}
            value={formData.zip}
          />
          <label htmlFor="city">City:</label>
          <input
            type="text"
            id="city"
            name="city"
            required
            onChange={handleChange}
            value={formData.city}
          /></div>
          <label htmlFor="message">Message:</label>
        <div class="message">
        <textarea
            id="message"
            name="message"
            required
            onChange={handleChange}
            value={formData.message}
          />
          <button type="submit">Submit</button>
        </div>
        </form>
        </>
      );
}

export default Home;

import { useState } from "preact/hooks";
//@ts-ignore
import { useRouter } from "wouter-preact";
import localforage from 'localforage';

function About() {
    const [data, setData] = useState([]);
    useState(async () => {
        const token = await localforage.getItem('authToken');
        fetch(import.meta.env.VITE_API_URL + "/get_all", {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        })
        .then(response => response.json())
        .then(data => {
            setData(data.data);
        })
    })

    return (
        <div class="app">
            <h1>All Stored Data</h1>
            <table>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>email</th>
                        <th>Straße & Hausnummer</th>
                        <th>PLZ</th>
                        <th>Stadt</th>
                        <th>Nachricht</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((item: any) => (
                        <tr>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td>{item.email}</td>
                            <td>{item.street}</td>
                            <td>{item.postal_code}</td>
                            <td>{item.city}</td>
                            <td>{item.message}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}


export default About;

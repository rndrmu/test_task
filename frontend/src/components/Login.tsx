import { useState } from "preact/hooks";
//@ts-ignore
import { useRouter } from "wouter-preact";
// localforage
import localforage from 'localforage';

function Login() {
    const [_, setLoggedIn] = useState(false);
    localforage.getItem('authToken').then((value) => {
        if (value) {
            setLoggedIn(true);
        }
    }).catch((err) => {
        console.log(err);
    });
    console.log(import.meta.env.VITE_API_URL);
    //
    return (
        <div class="login">
            <h1>Login</h1>
            <p id="loginNotice"></p>
            <form onSubmit={handleSubmit}>
                <label htmlFor="username">Email:</label>
                <input
                    type="email"
                    id="username"
                    name="username"
                    required
                />
                <label htmlFor="password">Password:</label>
                <input
                    type="password"
                    id="password"
                    name="password"
                    required
                />
                <button type="submit">Login</button>
            </form>
        </div>
    )
}

async function handleSubmit(e: any) {
    e.preventDefault();
    const formData = {
        email: e.target.username.value,
        password: e.target.password.value,
    };
    fetch(import.meta.env.VITE_API_URL + "/login", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
    })
    .then(response => response.json())
    .then(data => {
        if (data.data) {
            localforage.setItem('authToken', data.data).then(() => {
                window.location.href = '/';
            }).catch((err) => {
                console.log(err);
            });
        } else {
            document.getElementById('loginNotice')!.innerHTML = 'Login failed.';
        }
    })
}


export default Login;

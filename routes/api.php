<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/submit', [FormController::class, 'submit']);

// get all submitted forms (auth required)
Route::get('/get_all', [FormController::class, 'get_all'])->middleware('auth:api');

// Auth routes
Route::post('/login', [AuthController::class, 'login'])->name('api_login');
Route::middleware('auth:api')->post('/logout', [AuthController::class, 'logout'])->name('api_logout');
Route::post('/register', [AuthController::class, 'register'])->name('api_register');




